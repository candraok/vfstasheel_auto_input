Languange prerequisite:

* Install Python
* Make sure has the latest python version

  ![img](image/readme/1634092887819.png)

Framework component prerequisite:

* Install robot framework library, **pip install robotframework**
* Install request library, **pip install requests**
* Install robot framework request library, **pip install robotframework-requests**
* Install robot framework json library, **pip install -U robotframework-jsonlibrary**

![](image/readme/1634093521991.png)

Additional component:

* If you want to have an excel as DDT (Data Driven Testing), **pip install -U robotframework-datadriver[XLS]**

For more library information visit, https://robotframework.org/#libraries
