# Run Scenario = robot -v data:source -v notify:true tests\api\Schedule\get_appoinment_test.robot

*** Settings ***
Resource          ../../../templates/api/schedule/get_appoinment_template.robot
Resource         ../../../resources/api/token/get_token_keyword.robot
Resource         ../../../resources/api/token/get_audit_access_token_keyword.robot
Resource    ../../../templates/api/submit/check_visa_available_template.robot

*** Test Cases ***
Prerequisite - User login
    Get response token
    Get response audit token

Check appointment exist
    Prepare appointment URL
    Get appointment response
    Check normal appointment in Cipinang indah
    Check normal appointment in Jakarta_Epiwalk
    # Check lounge appointment in Cipinang indah
    Check lounge appointment in Jakarta_Epiwalk
    Send to telegram when appointment exist

# Check visa cipinang indah available
#     Prepare check visa cipinang indah URL
#     Get check visa cipinang indah response API
#     Check visa cipinang indah available
#     Send cipinang indah slot to telegram

# Check visa jakarta epiwalk available
#     Prepare check visa jakarta epiwalk URL
#     Get check visa jakarta epiwalk response API
#     Check visa jakarta epiwalk available
#     Send jakarta epiwalk slot to telegram