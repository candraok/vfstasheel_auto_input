# Get Appointment template

*** Settings ***
Resource    ../../../resources/api/schedule/get_appoinment_keyword.robot
Resource          ../../../resources/api/webhook/msg.robot

*** Keywords ***
Prepare appointment URL
    Setup appointment URL
    Log to Console    Result :

Get slot available
    [Arguments]    ${response}    ${search}
    ${isFound}	Call Method	${response}    find    ${search}
    Return From Keyword If	${isFound}>=0	True

Check slot available
    [Arguments]    ${response}
    @{status_list}=    Create List
    ${response_no}=    Get slot available    ${response}    ${CONFIG.Data.NOT_AVAILABLE1}
    ${response_all}=    Get slot available    ${response}    ${CONFIG.Data.NOT_AVAILABLE2}
    Append To List    ${status_list}    ${response_no}    ${response_all}
    ${string_list}    Convert To String    ${status_list}
    ${result}=    Get slot available    ${string_list}    True
    [Return]    ${result}

Get appointment response
    ${app_response}=    Run Keyword If    '${token}' != '${EMPTY}'    Get response
    Set Test Variable    ${app_response}

Check normal appointment in Cipinang indah
    Status Should Be    200    ${app_response}
    Log to Console    Cipinang Indah (normal) - ${app_response.json()}[cityVscDetailsList][0][normalSlotDate]
    ${exist}=    Check slot available    ${app_response.json()}[cityVscDetailsList][0][normalSlotDate]
    Set Suite Variable    ${exist_ci}    ${exist}
    IF    '${exist}' != 'True'
        Sum up Error    Cipinang Indah (normal) - ${app_response.json()}[cityVscDetailsList][0][normalSlotDate]
        ${set_time_ci}=    Set Time    ${app_response.json()}[cityVscDetailsList][0][normalSlotDate]
        Set Suite Variable    ${book_date_ci}    ${set_time_ci}
        Set Suite Variable    ${vscid_ci}    ${app_response.json()}[cityVscDetailsList][0][vsc][vscId]
    END

Check normal appointment in Jakarta_Epiwalk
    Status Should Be    200    ${app_response}
    Log to Console    Jakarta_Epiwalk (normal) - ${app_response.json()}[cityVscDetailsList][1][normalSlotDate]
    ${exist}=    Check slot available    ${app_response.json()}[cityVscDetailsList][1][normalSlotDate]
    Set Suite Variable    ${exist_je}    ${exist}
    IF    '${exist}' != 'True'
        Sum up Error    Jakarta_Epiwalk (normal) - ${app_response.json()}[cityVscDetailsList][1][normalSlotDate]
        ${set_time_je}=    Set Time    ${app_response.json()}[cityVscDetailsList][1][normalSlotDate]
        Set Suite Variable    ${book_date_je}    ${set_time_je}
        Set Suite Variable    ${vscid_je}    ${app_response.json()}[cityVscDetailsList][1][vsc][vscId]
    END

Check lounge appointment in Cipinang indah
    Status Should Be    200    ${app_response}
    Log to Console    Cipinang Indah (lounge) - ${app_response.json()}[cityVscDetailsList][0][silverSlotDate]
    ${exist}=    Check slot available    ${app_response.json()}[cityVscDetailsList][0][silverSlotDate]
    IF    '${exist}' != 'True'
        Sum up Error    Cipinang Indah (lounge) - ${app_response.json()}[cityVscDetailsList][0][silverSlotDate]
        # ${set_time_ci_l}=    Set Time    ${app_response.json()}[cityVscDetailsList][0][silverSlotDate]
        # Set Suite Variable    ${book_date_ci_l}    ${set_time_ci_l}
        # Set Suite Variable    ${vscid_ci_l}    ${app_response.json()}[cityVscDetailsList][0][vsc][vscId]
    END

Check lounge appointment in Jakarta_Epiwalk
    Status Should Be    200    ${app_response}
    Log to Console    Jakarta_Epiwalk (lounge) - ${app_response.json()}[cityVscDetailsList][1][silverSlotDate]
    ${exist}=    Check slot available    ${app_response.json()}[cityVscDetailsList][1][silverSlotDate]
    IF    '${exist}' != 'True'
        Sum up Error    Jakarta_Epiwalk (lounge) - ${app_response.json()}[cityVscDetailsList][1][silverSlotDate]
        # ${set_time_je_l}=    Set Time    ${app_response.json()}[cityVscDetailsList][1][silverSlotDate]
        # Set Suite Variable    ${book_date_je_l}    ${set_time_je_l}
        # Set Suite Variable    ${vscid_je_l}    ${app_response.json()}[cityVscDetailsList][1][vsc][vscId]
    END

Send to telegram when appointment exist
    Status Should Be    200    ${app_response}
    Send report to telegram