*** Settings ***
Resource          ../../../resources/api/submit/check_visa_available_keyword.robot
Resource          ../../../resources/api/webhook/msg.robot

*** Keywords ***
Prepare check visa cipinang indah URL
    IF    '${exist_ci}' != 'True'
        Setup check visa URL
    ELSE
        Skip
    END

Prepare check visa jakarta epiwalk URL
    IF    '${exist_je}' != 'True'
        Setup check visa URL
    ELSE
        Skip
    END

Get check visa cipinang indah response API
    ${app_response}=    Run Keyword If    '${token}' != '${EMPTY}'    Get check visa cipinang indah response
    Set Suite Variable    ${check_visa_response_ci}    ${app_response}

Check visa cipinang indah available
    Status Should Be    200    ${check_visa_response_ci}
    Set Suite Variable    ${slot_visa_ci}    ${check_visa_response_ci.json()}[slots]
    FOR    ${count}    IN RANGE    2
        Exit For Loop If    ${count} == 2
        Exit For Loop If    ${slot_visa_ci} != None
        Log To Console    ${slot_visa_ci}
        ${app_response}=    Run Keyword If    '${token}' != '${EMPTY}'    Get check visa cipinang indah response
        Set Suite Variable    ${slot_visa_ci}    ${app_response.json()}[slots]
    END

Get check visa jakarta epiwalk response API
    ${app_response}=    Run Keyword If    '${token}' != '${EMPTY}'    Get check visa jakarta epiwalk response
    Set Suite Variable    ${check_visa_response_je}    ${app_response}

Check visa jakarta epiwalk available
    Status Should Be    200    ${check_visa_response_je}
    Set Suite Variable    ${slot_visa_je}    ${check_visa_response_je.json()}[slots]
    FOR    ${count}    IN RANGE    2
        Exit For Loop If    ${count} == 2
        Exit For Loop If    ${slot_visa_je} != None
        Log To Console    ${slot_visa_je}   
        ${app_response}=    Run Keyword If    '${token}' != '${EMPTY}'    Get check visa jakarta epiwalk response
        Set Suite Variable    ${slot_visa_je}    ${app_response.json()}[slots]
    END
