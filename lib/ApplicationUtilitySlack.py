
from robot.api import logger as log
from robot.errors import *
from robot.libraries.BuiltIn import BuiltIn
import requests
import psycopg2
from robot.api.deco import keyword
from distutils.util import strtobool


class ApplicationUtilitySlack(object):
    ROBOT_AUTO_KEYWORDS = False
    ROBOT_LISTENER_API_VERSION = 3

    def __init__(self):
        self.ROBOT_LIBRARY_LISTENER = self
        self.builtIn = BuiltIn()
        self.test_status = {}
        self.suite_status = {}

    @keyword
    def run_keyword_and_set_slack_message_on_failure(self, msg, name, *args):
        try:
            self.builtIn.run_keyword(name, *args)
        except ExecutionFailed as err:
            log_name = self.builtIn.get_variable_value('${TEST NAME}')
            SLACK_LOG_TEMPLATE = self.builtIn.get_variable_value(
                '${SLACK_LOG_TEMPLATE}')
            self.__set_slack_message(SLACK_LOG_TEMPLATE, log_name, msg)
            if not err.dont_continue:
                err.continue_on_failure = True
            raise err

    @keyword
    def run_keyword_and_send_slack_message_on_failure(self, name, *args):
        try:
            self.builtIn.run_keyword(name, *args)
        except ExecutionFailed as err:
            log_name = self.builtIn.get_variable_value('${TEST NAME}')
            SLACK_LOG_TEMPLATE = self.builtIn.get_variable_value(
                '${SLACK_LOG_TEMPLATE}')
            self.__set_slack_message(SLACK_LOG_TEMPLATE, log_name, err.message)
            if not err.dont_continue:
                err.continue_on_failure = True
            raise err

    @keyword
    def send_slack_notification_results(self):
        notify = strtobool(self.builtIn.get_variable_value('${notify}'))

        SLACK_WEBHOOK_URL = self.builtIn.get_variable_value(
            '${CONFIG.Environment.SLACK_WEBHOOK_URL}')
        if notify:
            try:
                slack_data = self.__build_slack_message()
                if len(slack_data) != 0:
                    response = requests.post(
                        SLACK_WEBHOOK_URL, json=slack_data)
                    if response.status_code != 200:
                        raise ValueError(
                            'Request to slack returned an error %s, the response is:\n%s'
                            % (response.status_code, response.text)
                        )
            except ExecutionFailed as err:
                if not err.dont_continue:
                    err.continue_on_failure = True
                raise err

    def __build_slack_message(self):

        # Initial message set to no test error
        suite_name = self.builtIn.get_variable_value('${SUITE NAME}')
        # slack_data = {
        #     'text': f"Test Execution Result for {suite_name.rsplit('.')[0]} Scenario: \n:white_check_mark: It's all good to go",
        # }
        slack_data = {}

        global_variable = self.builtIn.get_variable_value('${global_variable}')
        if global_variable is not None:
            slack_data["username"] ="Robot Reporter"
            slack_data['text'] = self.builtIn.get_variable_value(
                '${global_variable}')

        # reportfile = self.builtIn.get_variable_value('${REPORT FILE}')
        # slack_data['text'] += '\n' + reportfile

        # results = {k: v for test_results in self.suite_status.values() for k, v in test_results.iteritems()}

        return slack_data

    def __set_slack_message(self, tmpl, logname, msg):
        msg = tmpl.format(case_name=logname, message=msg)
        global_variable = self.builtIn.get_variable_value('${global_variable}')
        if global_variable is not None:
            global_variable += '\n:small_blue_diamond:' + msg
            self.builtIn.set_global_variable(
                '${global_variable}', global_variable)
        else:
            suite_name = self.builtIn.get_variable_value('${SUITE NAME}')
            global_variable = f"Test Execution Result for {suite_name.rsplit('.')[0]} Scenario: "
            global_variable += '\n:small_blue_diamond:' + msg
            self.builtIn.set_global_variable(
                '${global_variable}', global_variable)

    def end_test(self, data, result):
        self.test_status[data] = result.passed
        # log.console(result.passed)

    def end_suite(self, data, result):
        self.suite_status[data] = self.test_status
        # log.console(self.test_status)
        self.test_status = {}

    def connect_mysql(self, query):
        connect = psycopg2.connect (
            database = self.builtIn.get_variable_value("${CONFIG.Environment.MYSQL_DB}"),
            user = self.builtIn.get_variable_value("${CONFIG.Environment.MYSQL_USERNAME}"),
            password=self.builtIn.get_variable_value("${CONFIG.Environment.MYSQL_KEY}"),
            host=self.builtIn.get_variable_value("${CONFIG.Environment.MYSQL_HOST}"),
            port=self.builtIn.get_variable_value("${CONFIG.Environment.MYSQL_PORT}")
        )

        cur = connect.cursor()
        result = cur.execute(query)
        connect.commit()
        connect.close()
        return result