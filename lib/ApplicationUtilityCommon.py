import os
import json
from robot.api import logger
from datetime import datetime, date, timedelta
import calendar
from urllib.parse import urlencode


# region DATE TIME HELPER
def substract_month_from_date(from_date, to_date):
    d1 = datetime.strptime(from_date, '%d %b %Y').date()
    print(d1)
    d2 = datetime.strptime(to_date, '%d %b %Y').date()
    print(d2)
    month_diff = (d2.year - d1.year) * 12 + d2.month - d1.month
    return month_diff + 1


def get_first_day_of_the_current_month():
    currentMonth = datetime.now().month
    currentYear = datetime.now().year
    first_day = date(currentYear, currentMonth, 1)
    return first_day.strftime("%Y-%m-%d")


def get_last_day_of_the_current_month():
    currentMonth = datetime.now().month
    currentYear = datetime.now().year
    _, num_days = calendar.monthrange(currentYear, currentMonth)
    last_day = date(currentYear, currentMonth, num_days)
    return last_day.strftime("%Y-%m-%d")


def get_current_date():
    return datetime.today().strftime('%Y-%m-%d')


def yesterday():
    today = date.today()
    yesterday = today - timedelta(days=1)
    return yesterday


def today():
    today = date.today()
    return today
# endregion

# region FILE HELPER


def file_exists(file):
    return os.path.isfile(file)
# endregion


# region QUERY STRING BUILDER
param = {}


def set_query_string(str, val):
    param[str] = val


def clear_query():
    param.clear()


def build_query():
    query_string = urlencode(param)
    return query_string
# endregion

# region JSON HELPER


def update_json_bulk(f, update_map):
    with open(f, "r+") as jsonFile:
        data = json.load(jsonFile)

        if len(update_map) == 0:
            logger.info("No Update to JSON keys")
        else:
            for k, v in update_map.items():
                data[k] = v

        jsonFile.seek(0)  # rewind
        json.dump(data, jsonFile)
        jsonFile.truncate()


def update_json_single(f, k, v):
    with open(f, "r+") as jsonFile:
        data = json.load(jsonFile)

        data[k] = v

        jsonFile.seek(0)  # rewind
        json.dump(data, jsonFile)
        jsonFile.truncate()
# endregion


def set_time(v):
    date = str(v).split(" ")[0]
    month = str(v).split(" ")[1]

    if len(date) == 1:
        finaldate = '0' + date
        return finaldate + '-' + month + '-2022'

    return date + '-' + month + '-2022'

def get_current_time():
    now = datetime.now()
    current_time = now.strftime("%H:%M")
    return current_time