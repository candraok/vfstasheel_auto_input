import configparser as cg
import os
from robot.api import FatalError


def get_variables(env):
    varname = "CONFIG"
    filename = ""
    base_dir = "../config/env"

    # if env.lower() == "dev":
    #     filename = os.path.abspath(os.path.join(
    #         os.path.dirname(__file__), base_dir, "dev.ini"))
    # elif env.lower() == "uat":
    #     filename = os.path.abspath(os.path.join(
    #         os.path.dirname(__file__), base_dir, "uat.ini"))
    # else:
    #     filename = os.path.abspath(os.path.join(
    #         os.path.dirname(__file__), base_dir, f'{env}.ini'))

    filename = os.path.abspath(os.path.join(
            os.path.dirname(__file__), base_dir, f'{env}.ini'))

    config = cg.ConfigParser()
    config.read(filename)

    variables = {}
    if len(config.sections()) == 0:
        raise FatalError(
            "No proper environment configuration detected, please make sure you have valid path to your .ini file")

    for section in config.sections():
        for key, value in config.items(section):
            var = "%s.%s.%s" % (varname, section, key)
            variables[var] = value
    return variables
