# Get VISA list

*** Settings ***
Resource          ../../../config/config.robot

*** Keywords ***
Setup check visa URL
    Set Test Variable    ${main_url}    ${CONFIG.Data.MAIN_URL}/VFSTasheel/services/makeappointment/checkvisaavailable

Get check visa cipinang indah response
    ${startTime}=    Get Current Time
    ${headers}=    Create Dictionary    origin=${CONFIG.Data.ORIGIN}    content-type=${CONFIG.Data.CONTENT_TYPE}    x-vfs-authentication=${token}    referer=${CONFIG.Data.ORIGIN}    Host=${CONFIG.Data.HOST}    User-Agent=${CONFIG.Data.USER_AGENT}
    ${body}=    Create Dictionary    countryId=${CONFIG.Data.COUNTRY_ID}    visaTypeId=${CONFIG.Data.VISA_TYPE_ID}    vscId=${vscid_ci}    selectedAppDate=${book_date_ci}    bookingMode=Online    processType=Full    appointmentType=Normal    currentBookingDate=${book_date_ci}    noOfApplicants=1    startTime=${startTime}    showWaitinglistQuota=false    isUpgrade=n    tokenId=${audit_token}
    ${response}=    POST    url=${main_url}    headers=${headers}    json=${body}
    [Return]    ${response}

Get check visa jakarta epiwalk response
    ${startTime}=    Get Current Time
    ${headers}=    Create Dictionary    origin=${CONFIG.Data.ORIGIN}    content-type=${CONFIG.Data.CONTENT_TYPE}    x-vfs-authentication=${token}    Host=${CONFIG.Data.HOST}    User-Agent=${CONFIG.Data.USER_AGENT}    referer=${CONFIG.Data.ORIGIN}
    ${body}=    Create Dictionary    countryId=${CONFIG.Data.COUNTRY_ID}    visaTypeId=${CONFIG.Data.VISA_TYPE_ID}    vscId=${vscid_je}    selectedAppDate=${book_date_je}    bookingMode=Online    processType=Full    appointmentType=Normal    currentBookingDate=${book_date_je}    noOfApplicants=1    startTime=${startTime}    showWaitinglistQuota=false    isUpgrade=n    tokenId=${audit_token}
    ${response}=    POST    url=${main_url}    headers=${headers}    json=${body}
    [Return]    ${response}
