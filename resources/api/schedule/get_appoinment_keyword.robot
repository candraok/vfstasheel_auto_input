# Get Available Appointment Schedule

*** Settings ***
Resource          ../../../config/config.robot

*** Keywords ***
Setup appointment URL
    Set Test Variable    ${main_url}    ${CONFIG.Data.MAIN_URL}/VFSTasheel/services/makeappointment/getcityandvsc

Get response
    ${headers}=    Create Dictionary    origin=${CONFIG.Data.ORIGIN}    content-type=${CONFIG.Data.CONTENT_TYPE}    x-vfs-authentication=${token}    referer=${CONFIG.Data.ORIGIN}    Host=${CONFIG.Data.HOST}    User-Agent=${CONFIG.Data.USER_AGENT}
    ${body}=    Create Dictionary    countryId=${CONFIG.Data.COUNTRY_ID}    visaTypeId=${CONFIG.Data.VISA_TYPE_ID}    governorateId=null    language=${CONFIG.Data.LANGUAGE}
    ${response}=    POST    url=${main_url}    headers=${headers}    json=${body}
    [Return]    ${response}

