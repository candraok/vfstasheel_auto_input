# Prerequisite GET audit access token

*** Settings ***
Resource          ../../../config/config.robot

*** Keywords ***
Setup audit token URL
    Set Test Variable    ${token_url}    ${CONFIG.Data.MAIN_URL}/ref/get_audit_access_token

Get audit token response
    ${headers}=    Create Dictionary    origin=${CONFIG.Data.ORIGIN}    x-vfs-authentication=${token}    referer=${CONFIG.Data.ORIGIN}    Host=${CONFIG.Data.HOST}    User-Agent=${CONFIG.Data.USER_AGENT}
    ${body}=    Create Dictionary    countryId=${CONFIG.Data.COUNTRY_ID}    languageId=${CONFIG.Data.LANGUAGE}
    ${response}=    POST    url=${token_url}    headers=${headers}    json=${body}
    [Return]    ${response}

Get response audit token
    Setup audit token URL
    ${token_response}=    Get audit token response
    ${is_token_exist}=    Evaluate    '${token_response.json()}[token]' != '${EMPTY}'
    Set Suite Variable    ${audit_token}    ${token_response.json()}[token]

