# Prerequisite GET user's token

*** Settings ***
Resource          ../../../config/config.robot

*** Keywords ***
Setup URL
    Set Test Variable    ${token_url}    ${CONFIG.Data.MAIN_URL}/ref/gettoken

Get token response
    ${headers}=    Create Dictionary    origin=${CONFIG.Data.ORIGIN}    referer=${CONFIG.Data.ORIGIN}    Host=${CONFIG.Data.HOST}    User-Agent=${CONFIG.Data.USER_AGENT}
    ${response}=    GET    url=${token_url}    headers=${headers}
    [Return]    ${response}

Get response token
    Setup URL
    ${token_response}=    Get token response
    ${is_token_exist}=    Evaluate    '${token_response.json()}[token]' != '${EMPTY}'
    Set Suite Variable    ${token}    ${token_response.json()}[token]

