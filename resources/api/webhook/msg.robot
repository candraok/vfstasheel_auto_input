*** Settings ***
Library    RequestsLibrary
# SUM UP Report 

*** Variables ***
${notify}         false

*** Keywords ***
Set GlobalVar
    [Arguments]    ${var}
    ${global_variable}    Set Variable    ${var}
    Set Global Variable    ${global_variable}

Check Global exist
    ${exist}    Run Keyword And Return Status    Get Variables    ${global_variable}
    [Return]    ${exist}

Sum up Error
    [Arguments]    ${error}
    ${check_var}=    Check Global exist
    IF    '${check_var}' == 'True'
        ${global_variable}=    Set Variable    ${global_variable}\n🔹 ${error}
        Set Global Variable    ${global_variable}
    ELSE
        Set GlobalVar    ALERT :
        ${global_variable}=    Set Variable    ${global_variable}\n🔹 ${error}
        Set Global Variable    ${global_variable}
    END

Send report to telegram
    IF    '${notify}' == 'true'
        ${check_var}=    Check Global exist
        IF    '${check_var}' == 'True'
            ${url}=    Set Variable    ${CONFIG.Data.TELEGRAM_WEBHOOK_URL}text=${global_variable}
            ${url2}=    Set Variable    ${CONFIG.Data.TELEGRAM_WEBHOOK_URL2}text=${global_variable}
            POST    url=${url}
            POST    url=${url2}
        END
    END
    
Send cipinang indah slot to telegram
    ${url3}=    Set Variable    ${CONFIG.Data.TELEGRAM_WEBHOOK_URL2}text=Slot:[${slot_visa_ci}]
    POST    url=${url3}

Send jakarta epiwalk slot to telegram
    ${url4}=    Set Variable    ${CONFIG.Data.TELEGRAM_WEBHOOK_URL2}text=Slot:[${slot_visa_je}]
    POST    url=${url4}