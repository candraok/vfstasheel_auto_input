*** Settings ***
Library           RequestsLibrary
Library           Collections
Library           OperatingSystem
Library           ../lib/ApplicationUtilityCommon.py
# Library           JSONLibrary
Variables         ../lib/environment_configurations.py    ${data}

*** Variables ***
${SLACK_LOG_TEMPLATE}    Test case "{case_name}" | {message}
